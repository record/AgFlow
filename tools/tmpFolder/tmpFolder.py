#!/usr/bin/env python

from optparse import OptionParser

import sys
import os
import shutil

def main(argv):
    parser = OptionParser()
    parser.add_option("-1", "--file_1", dest="firstFileName",
                      help="first file", metavar="FILE")
    parser.add_option("-2", "--file_2", dest="secondFileName",
                      help="second file", metavar="FILE")
    parser.add_option("-d", "--tempFolder", dest="temporaryFolder",
                      help="temporaryFolder", metavar="FILE")
    parser.add_option("-m", "--message", dest="message",
                      help="message", metavar="STRING")
    parser.add_option("-o", "--output", dest="outputFile",
                      help="outputFile", metavar="FILE")
    parser.add_option("-q", "--quiet",
                      action="store_false", dest="verbose", default=True,
                      help="don't print status messages to stdout")

    (options, args) = parser.parse_args()

    directory = options.temporaryFolder

    file1 = os.path.abspath(options.firstFileName)
    file2 = os.path.abspath(options.secondFileName)

    output = options.outputFile

    try:
        os.stat(directory)
    except:
        os.mkdir(directory)

    shutil.copy(file1, directory+'/climat.txt')
    shutil.copy(file2, directory+'/soil.txt')

    fileo = open(output, "w")

    fileo.write(os.path.abspath(directory)+'\n')
    fileo.write('--------------\n')

    fileo.write(options.message+'\n')
    fileo.write('--------------\n')

    # parcours du dossier temporaire
    for filename in os.listdir(os.path.abspath(directory)):
        # decompte du nombre de lignes
        n = sum(1 for line in open(os.path.abspath(directory+'/'+filename)))
        # stockage du nom du fichier et du nombre de lignes
        fileo.write(os.path.abspath(directory+'/'+filename) + ' '+ str(n) +'\n')
        fileo.write('--------------\n')

    fileo.close()

if __name__ == "__main__":
   main(sys.argv[1:])
