#!/bin/sh

### Run R providing the R script in $1 as standard input and passing
### the remaining arguments on the command line

# Function that writes a message to stderr and exits
function fail
{
    echo "$@" >&2
    exit 1
}

# Ensure R executable is found
which vle > /dev/null || fail "'vle' is required by this tool but was not found on path"

# Extract first argument
#infile=$1; shift

# Ensure the file exists
#test -f $infile || fail "R input file '$infile' does not exist"

# Invoke R passing file named by first argument to stdin

#vle -V7 -P "$1" list 

echo "------------------------"

#ls "$VLE_HOME"

echo "------------------------"

#ls "$VLE_HOME/pkgs-2.0"

echo "------------------------"

#ls "$VLE_HOME/pkgs-2.0/record.meteo_test"

echo "------------------------"

#ls "$VLE_HOME/pkgs-2.0/record.meteo_test/exp"

echo "------------------------"

#vle -i

echo "------------------------"

#env
#echo vle -V7 -P "$1" "$2"
#vle -V7 -P "$1" "$2"
vle -P "$1" "$2" &> /dev/null
#vle -P "$1" "$2"
mv test_meteo_view.csv $3

