#!/bin/env python
#Estelle Ancelet

import pandas, argparse, os

parser = argparse.ArgumentParser()
parser.add_argument("-l","--result_list", required=True)
parser.add_argument("-p","--file_prefix", required=True)
parser.add_argument("-f","--result_folder",required=True)
args = parser.parse_args()

print 'Get result list...'
resultList = pandas.read_csv(args.result_list, sep=',', index_col=0, header=None)

print 'Change file names...'
count=0
for index, experiment in resultList.iterrows():
    os.rename(args.result_folder + "/" + args.file_prefix + "-" + str(count) + ".csv",
        args.result_folder + "/" + index + ".csv")
    count += 1
print 'File names changed'
