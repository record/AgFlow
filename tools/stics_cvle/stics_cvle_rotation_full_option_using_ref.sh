#!/bin/sh

#$ -o /work/galaxy-vle/tmp/logcvle.txt
#$ -e /work/galaxy-vle/tmp/logcvle.txt
#$ -q workq
#$ -b n
#$ -R y
#$ -l h_vmem=64G 
#$ -l mem=10G
#$ -pe parallel_fill 4
#$ -M estelle.ancelet@inra.fr
#$ -m bea

echo "new job starting:"
date

n_slots=${GALAXY_SLOTS}
csv_plan=$1
sim_number=$(($(cat ${csv_plan} | wc -l) - 1))
#b_size=$(($(wc -c <$csv_plan)/10000))
# cvle mobilize one slot for the master so the simulations run on one slot less than what is given
estimated_b_size=$(($sim_number / ($n_slots-1)))
right=$(($sim_number % $n_slots))
if [ $right -gt 0 ] ; then 
    estimated_b_size=$estimated_b_size+1
fi
b_size=$(($estimated_b_size >= 1 ? $estimated_b_size : 1))
if [ $b_size -gt 100 ] ; then
    b_size=100
fi
block_number=$((${sim_number} / ${b_size}))
echo "number of GALAXY_SLOTS: ${GALAXY_SLOTS}"
echo "expected simulations: ${sim_number}"
echo "cvle block size : ${b_size}"
echo "cvle block expected numbers : ${block_number}"

echo "Copy tempopar.sti to working directory..."
cp $2 tempopar.sti 

echo "Unzip crop, soil and technical management archives..."
folderSoil="soil"
folderPlt="plt"
folderItk="itk"

unzip $3 
unzip -j $5 -d $folderPlt
unzip -j $6 -d $folderSoil
unzip -j $7 -d $folderItk

# particular case of weather dataset that can come from the user (.zip) or be stored in the galaxy instance
if [[ $4 =~ \.dat$ ]]; then 
    echo "Unzip weather archive..."
    folderMeteo="meteo"
    unzip -j $4 -d $folderMeteo
else
    folderMeteo=$4
fi

workFold=$(pwd)

echo "Check experiment plan..."
Rscript "$8/convert_usms_allsteps.R" $csv_plan $folderSoil $folderMeteo $folderItk $folderPlt $8 $workFold >& logr.txt

echo "starting simulation runs..."
outFile="outCvle.csv"
date
/usr/bin/time -v mpirun -d -v -np $n_slots --mca ras_gridengine_verbose 1 --mca plm_gridengine_verbose 1 --mca ras_gridengine_show_jobid 1 --mca plm_gridengine_debug 1 cvle --withoutspawn -b $b_size -i inputs_usms_filtered_completed.csv -o outFile -P Stics4AgFlow  stics_rotation_cvle_full_option.vpz >& logmpi.txt # run all simulations
#/usr/bin/time -v mpirun -d -v --mca ras_gridengine_verbose 1 --mca plm_gridengine_verbose 1 --mca ras_gridengine_show_jobid 1 --mca plm_gridengine_debug 1 cvle --withoutspawn -b $b_size -i inputs_usms_filtered_completed.csv -o outFile -P Stics4AgFlow  stics_rotation_cvle_full_option.vpz >& logmpi.txt # run all simulations
echo "ending simulation runs... exit status: $?"
date

filePrefix="stics_cvle_results" 
folderResults="sticsResults" 

rm -rf $folderResults
mkdir $folderResults
mv $filePrefix* $folderResults

echo "Preparing result list..."
python "$8/handle_result_list.py" -l outFile -p $filePrefix -f $folderResults

echo "Preparing results archive..."
zip -r $folderResults $folderResults
mv $folderResults.zip $9
#rm -r $folderResults

