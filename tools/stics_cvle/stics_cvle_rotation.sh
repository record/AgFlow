#!/bin/sh

#$ -o /work/galaxy-vle/tmp/logcvle.txt
#$ -e /work/galaxy-vle/tmp/logcvle.txt
#$ -q workq
#$ -b n
#$ -R y
#$ -l h_vmem=64G 
#$ -l mem=10G
#$ -pe parallel_fill 4
#$ -M estelle.ancelet@inra.fr
#$ -m bea

echo "new job starting:"
date

# unzip climat archive
unzip $2

csv_plan=$3
b_size=2

echo "number of nodes used: ${NSLOTS}"

sim_number=$(($(cat ${input_csv} | wc -l) - 1))
echo "expected simulations: ${sim_number}"

echo "cvle block size : ${b_size}"

block_number=$(( ${sim_number} / ${b_size}))

echo "cvle block expected numbers : ${block_number}"

echo "starting simulation runs..."
date
/usr/bin/time -v mpirun -d -v -np 4 --mca ras_gridengine_verbose 1 --mca plm_gridengine_verbose 1 --mca ras_gridengine_show_jobid 1 --mca plm_gridengine_debug 1 cvle --withoutspawn -b ${b_size} -i ${csv_plan} -o $4 -P Stics4AgFlow $1 >& logmpirun.txt # run all simulations
echo "ending simulation runs... exit status: $?"
date

