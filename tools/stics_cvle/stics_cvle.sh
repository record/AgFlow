#!/bin/sh

#$ -o /work/galaxy-vle/tmp/logcvle.txt
#$ -e /work/galaxy-vle/tmp/logcvle.txt
#$ -q workq
#$ -b n
#$ -R y
#$ -l h_vmem=64G 
#$ -l mem=10G
#$ -pe parallel_fill 4
#$ -M estelle.ancelet@inra.fr
#$ -m bea

echo "new job starting:"
date

input_csv=$1
b_size=2

# untar meteo archive
tar -xzf $2

# make a symlink to tempopar.sti, expected by .vpz
ln -s $5 tempopar.sti

# unzip soil archive
unzip $3

# unzip usm archive and move it to the usm directory
mkdir usm/
unzip $4
# move to unzipped directory
cd `unzip -Z -1 $4 | head -1`
mv * ../usm/
cd ..

echo "number of nodes used: ${NSLOTS}"

sim_number=$(($(cat ${input_csv} | wc -l) - 1))
echo "expected simulations: ${sim_number}"

echo "cvle block size : ${b_size}"

block_number=$(( ${sim_number} / ${b_size}))

echo "cvle block expected numbers : ${block_number}"

echo "starting simulation runs..."
date
/usr/bin/time -v mpirun -d -v -np 4 --mca ras_gridengine_verbose 1 --mca plm_gridengine_verbose 1 --mca ras_gridengine_show_jobid 1 --mca plm_gridengine_debug 1 cvle --withoutspawn -b ${b_size} -i ${input_csv} -o $6 -P Stics4AgFlow stics_storage-view.vpz >& logmpirun.txt # run all simulations
echo "ending simulation runs... exit status: $?"
date

