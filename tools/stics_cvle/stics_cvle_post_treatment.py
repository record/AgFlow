#!/bin/env python
#Estelle Ancelet
''' handle cvle output file to get only one file with the structure : time | LAI1 | LAI2 | LAI3 etc.
'''

import pandas, argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f","--file", required=True, help="File to process (csv from CVLE)")
parser.add_argument("-o","--output", required=True, help="The name of the output csv with horizontal data")
args = parser.parse_args()

cvleColumns = ['time','lai']
horizontalData = pandas.DataFrame(index=None)

cvleData = pandas.read_csv(args.file, sep=' ', names = cvleColumns, header=None, skip_blank_lines=False)

simBegin = 0

print 'Parsing CVLE csv...'
for index, row in cvleData.iterrows():
    if pandas.isnull(row['time']):
        if simBegin == 0:
            horizontalData['time'] = cvleData.loc[simBegin+2 : index-1,'time'].reset_index(drop=True)
        simId = 'lai_' + cvleData.loc[simBegin,'time']
        horizontalData[simId] = cvleData.loc[simBegin+2 : index-1,'lai'].reset_index(drop=True)
        simBegin = index + 1

# last dataset
# depending on the system, there possibly is no empty trailing row
# TODO : create a function to avoid repetition
if (simBegin < len(cvleData)):
    simId = 'lai_' + cvleData.loc[simBegin,'time']
    horizontalData[simId] = cvleData.loc[simBegin+2 : len(cvleData),'lai'].reset_index(drop=True)

print 'Writing csv...'
horizontalData.to_csv(args.output, sep='	', header=True, index=False)

