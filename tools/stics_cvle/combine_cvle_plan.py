#!/bin/env python
#Estelle Ancelet
''' create cvle plan .csv
'''

import pandas, argparse

parser = argparse.ArgumentParser()
parser.add_argument("-w","--weather_list", required=True)
parser.add_argument("-s","--soil_list", required=True)
parser.add_argument("-i","--itk_list", required=True)
parser.add_argument("-p","--cvle_plan", required=True)
parser.add_argument("-e","--error_file", required=True)
args = parser.parse_args()

print 'Get input lists...'
weatherList = pandas.read_csv(args.weather_list, sep=',', index_col=0)
soilList = pandas.read_csv(args.soil_list, sep=',', index_col=0)

# list to store the plan lines with problems and get them back to the user
errorList = pandas.Series()
okList = []

print 'Reading input plan...'
itkList = pandas.read_csv(args.itk_list, sep=',', header=0, index_col = 0, low_memory=False)


print 'Adding columns...'
fWeather = 'cond_Stics.FClimat'
fSoil = 'cond_Stics.FSol'
itkList.insert(4, fWeather, 'Nothing')
itkList.insert(3, fSoil, 'Nothing')
#weatherFiles = pandas.Series()
#soilFiles = pandas.Series()

print 'Matching file lists'
for index, experiment in itkList.iterrows():
    noError = True
    try:
        #weatherFiles[index] = weatherList.loc[int(experiment.station.replace('_sta.txt',''))][0] # eancelet 2018-06-01 : solution above is faster
        itkList.loc[index,fWeather] = weatherList.loc[int(experiment.station.replace('_sta.txt',''))][0]
    except KeyError:
        errorList[index] = 'Weather file missing in file list.'
        noError = False
    except ValueError:
        errorList[index] = 'Station file miswritten (ex : 123456_sta.txt, with 123456 as cell id).'
        noError = False        
    try:
        #soilFiles[index] = soilList.loc[experiment.uts][0]
        itkList.loc[index,fSoil] = soilList.loc[experiment.uts][0]
    except KeyError:
        errorList[index] += ' Soil file missing.'
        noError = False
    if noError:
        okList.append(index)                  

print 'Writing csv with lines containing errors...'    
errorPlan = itkList.filter(items = errorList.index, axis=0) # axis=0 to filter rows
errorPlan.insert(0, 'error', errorList)
errorPlan.drop(fWeather, axis=1, inplace=True)
errorPlan.drop(fSoil, axis=1, inplace=True)
errorPlan.to_csv(args.error_file, sep=',', header=True, index=True)

print 'Writing result csv...'
itkList = itkList.filter(items = okList, axis=0)
#itkList.insert(4,'cond_Stics.FClimat', weatherFiles)
#itkList.insert(3,'cond_Stics.FSol', soilFiles)
itkList.to_csv(args.cvle_plan, sep=',', header=True, index=True)


