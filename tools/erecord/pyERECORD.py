#!/usr/bin/env python

from optparse import OptionParser

import sys
import pycurl
import io
import json
import csv, codecs, cStringIO

def send_post_and_receive(url, inputdata):
    """Send POST request and return response datas"""
    #
    buffer = io.BytesIO()
    c = pycurl.Curl()
    c.setopt(c.POST, 1)
    c.setopt(c.HTTPHEADER, ['Content-Type: application/json'])
    c.setopt(c.URL, url)
    #c.setopt(c.FOLLOWLOCATION, True)
    json_inputdata = json.dumps( inputdata)
    c.setopt(c.POSTFIELDS, json_inputdata)
    c.setopt(c.WRITEFUNCTION, buffer.write)
    c.perform()
    buffer_str = buffer.getvalue()
    buffer.close()
    buffer_str = buffer_str.decode("utf8")
    responsedata = json.loads(buffer_str)
    return responsedata

def content_simulation_results_tree(res, plan, restype):
    """Content of simulation results (res) in 'tree' style of presentation,
    according to plan values ('single', 'linear') and
    restype values ('dataframe' or 'matrix')
    """
    #
    res = json.loads(res)

    print("plan, restype :", plan, restype)
    print("res :", res)
    print("\nDetailing the results :")
    if restype=='dataframe' and plan=='single' :
        print("(view name, output data name, value)")
        for viewname,outputs in res.items() :
            for outputname,val in outputs.items() :
                print("- ", viewname, outputname, val)



    elif restype=='dataframe' and plan=='linear' :
        for (a,res_a) in enumerate(res) :
            print("*** simulation number ", a, ":")
            print("(view name, output data name, value)")
            for viewname,outputs in res_a.items() :
                for outputname,val in outputs.items() :
                    print("- ", viewname, outputname, val)
    elif restype=='matrix' and plan=='single' :
        print("(view name, value)")
        for viewname,v in res.items() :
            print("- ", viewname, v)
    elif restype=='matrix' and plan=='linear' :
        for (a,res_a) in enumerate(res) :
            print("*** simulation number ", a, ":")
            print("(view name, value)")
            for viewname,v in res_a.items() :
                print("- ", viewname, v)
    else : # error (unexpected)
        pass

def content_to_output(options,res) :
    res = json.loads(res)

    fieldnames = []

    valLen = 0;

    for viewname,outputs in res.items() :
        for outputname,val in outputs.items() :
            splitedout = outputname.split('.')
            fieldnames.append(splitedout[-1])
            valLen = len(val)

    with open(options.filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for i in range(0, valLen):
            dict = {}
            for viewname,outputs in res.items() :
                for outputname,val in outputs.items() :
                    varName = outputname.split('.')[-1]
                    dict[varName] = val[i]
            writer.writerow(dict)

def main(argv):
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename",
                      help="write report to FILE", metavar="FILE")
    (options, args) = parser.parse_args()

    inputdata = {"vpz":266, "format":"json"}
    inputdata["duration"] = 100
    inputdata["begin"] = 2453982.0

    #----------------------
    # parameters and mode in case of single plan (single simulation)

    # some parameters modification with 'pars'
    parameter_A = {"selection_name":"cond_wwdm.A",
                   "cname":"cond_wwdm","pname":"A",
                   "value":0.0064}
    parameter_Eb = {"selection_name":"cond_wwdm.Eb",
                    "cname":"cond_wwdm","pname":"Eb",
                    "value":1.86}
    pars = list()
    pars.append(parameter_A)
    pars.append(parameter_Eb)
    inputdata["pars"] = pars

    inputdata["mode"] = ["dataframe"]
    # or inputdata["mode"] = ["tree", "single", "matrix"]

    #----------------------
    # parameters and mode in case of linear plan (multiple simulation)

    # some parameters modification with 'pars' (multiple values)
    parameter_A = {"selection_name":"cond_wwdm.A",
                   "cname":"cond_wwdm","pname":"A",
                   "value":0.0064}
    parameter_Eb = {"selection_name":"cond_wwdm.Eb",
                    "cname":"cond_wwdm","pname":"Eb",
                    "value":1.84}
    pars = list()
    pars.append(parameter_A)
    pars.append(parameter_Eb)
    inputdata["pars"] = pars

    inputdata["mode"] = ["dataframe"]

    # or inputdata["mode"] = ["tree", "linear", "matrix"]

    #----------------------

    inputdata["outselect"] = "all"

    responsedata = send_post_and_receive(
        url="http://erecord.toulouse.inra.fr:8000/vpz/output/",
        inputdata=inputdata)

    keys = responsedata.keys()

    #######################################################
    # responsedata in case of 'tree' style of presentation
    #######################################################

    # id as VpzOutput
    if "id" in keys :
        id = responsedata['id']

    if 'res' in keys and 'plan' in keys and 'restype' in keys :
        #content_simulation_results_tree( res=responsedata['res'],
        #                                 plan=responsedata['plan'],
        #                                 restype=responsedata['restype'])

        content_to_output(options, res=responsedata['res'])

    pass

if __name__ == "__main__":
    main(sys.argv[1:])
