import pandas
import argparse
import random

print('Get arguments...')

parser = argparse.ArgumentParser()
parser.add_argument("-u","--unit", required=True, help="Unit of your datas")
parser.add_argument("-f", "--file", required=True, help="Name of your file")
args = parser.parse_args()


print('Creating data frame...')

planColumns = ['time', 'value', 'unit']
planData = pandas.DataFrame(index=None, columns=planColumns)

for i in range(0, 1000):
    planData = planData.append({'time' : i,
                                'value' : random.randint(0,i),
                                'unit' : args.unit}, ignore_index=True)


print('Creating file...')

planData.to_csv(args.file, sep=',', header=True, index=False)



