#!/usr/bin/env python

from optparse import OptionParser
from datetime import datetime, date, time
from datetime import timedelta

import sys
import csv, codecs, cStringIO
import os
import shutil

def main(argv):
    parser = OptionParser()
    parser.add_option("-p", "--plan", dest="plan",
                      help="csv plan", metavar="FILE")
    parser.add_option("-c", "--collection", dest="collection",
                      help="collection", metavar="STRING")
    parser.add_option("-n", "--names", dest="names",
                      help="names", metavar="STRING")
    parser.add_option("-d", "--dir", dest="dir",
                      help="tmp Dir", metavar="FILE")
    parser.add_option("-m", "--message", dest="message",
                      help="message", metavar="STRING")


    (options, args) = parser.parse_args()

    print options.message

    directory = options.dir

    try:
        os.stat(directory)
    except:
        os.mkdir(directory)

    collection = options.collection.split(':')
    names = options.names.split(':')

    collecByNames = {}

    for i in range(len(collection)):
        collecByNames[names[i]] = collection[i]

    with open(options.plan, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in spamreader:
            print ', '.join(row)
            print row[0]
            print row[1]
            shutil.copy(collecByNames[row[1]], directory+'/meteo'+'.'+row[0]+'.txt')

if __name__ == "__main__":
   main(sys.argv[1:])
