# !/bin/env python
# Estelle Ancelet
# 

import pandas
import argparse
import os
import zipfile

##################################################################################################################################################
#
# FUNCTIONS
#
##################################################################################################################################################


##################################################################################################################################################
#
# PROCESS
#
##################################################################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument("-a","--stics_output_archive", required=True)
parser.add_argument("-v","--variables", required=True)
parser.add_argument("-d","--drain_values", required=True)
args = parser.parse_args()

# creating aggregated file
data = pandas.DataFrame()

print 'Processing Stics output...'
with zipfile.ZipFile(args.stics_output_archive) as sticsOutZip:
    for member in sticsOutZip.namelist():
        outFileName = os.path.basename(member)
        # skip directories
        if not outFileName:
            continue
        
        outFile = sticsOutZip.open(member)  
        outSerie = pandas.read_csv(outFile, sep=';', index_col=0, header=0)
        outSerie = outSerie.drop(outSerie.index[0]) # remove first line of simulation with 0 values
        outSerie = outSerie.groupby(['top:meteo.Annee'])['top,parcelle:SticsOut.drain'].sum()
        expId = outFileName.replace('.csv','')
        data[expId] = outSerie

        # eancelet 2018-06-07, TODO : add new indexes if needed (outSerie.loc['2004']=14.23)
    sticsOutZip.close()

print 'Creating aggregate file...'
data.to_csv(args.drain_values, sep=',', header=True, index=True)

