#!/bin/sh

#$ -o /work/galaxy-vle/tmp/logcvle.txt
#$ -e /work/galaxy-vle/tmp/logcvle.txt
#$ -q workq
#$ -b n
#$ -R y
#$ -l h_vmem=64G 
#$ -l mem=10G
#$ -pe parallel_fill 4
#$ -M estelle.ancelet@inra.fr
#$ -m bea

echo "new job starting:"
date

#Load required modules and environment variables
#source /save/galaxy-vle/DEV/config_genotoul_vle-2.0.dev

#oDir="/work/galaxy-vle/tmp"
input_csv="/work/galaxy-vle/my_tools/wwdm_cvle/plan.csv"
b_size=2

#mkdir ${oDir}
#cd ${oDir}


echo "number of nodes used: ${NSLOTS}"
#echo "output directory used: ${oDir}"

sim_number=$(($(cat ${input_csv} | wc -l) - 1))
echo "expected simulations: ${sim_number}"

echo "cvle block size : ${b_size}"

block_number=$(( ${sim_number} / ${b_size}))

echo "cvle block expected numbers : ${block_number}"

echo "starting simulation runs..."
date
/usr/bin/time -v mpirun -d -v -np 4 --mca ras_gridengine_verbose 1 --mca plm_gridengine_verbose 1 --mca ras_gridengine_show_jobid 1 --mca plm_gridengine_debug 1 cvle --withoutspawn -b ${b_size} -i ${input_csv} -o out.txt -P wwdm wwdm.vpz >& logmpirun.txt # run all simulations
echo "ending simulation runs..."
date

mv out.txt $1

