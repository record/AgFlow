.. class:: infomark

This tool gets data from Infosol unit datamark and creates an archive with param.sol files formatted for Stics model. It provides also a soil list in the expected format for Cvle and a tempopar with pebble type definitions.

-----

**Examples**

- Choose a crop type: *fiels crops, permanent grassland...*

- Output 1: zip of param.sol in the format expected by stics

    1 sol_3521450_gc 45.0 0.14 30 0 6.5 0.01 0.20 11.6 0 80 50 0.5 60 5 0.01 11 0.33
    1 2 2 2 2 2 1 1
    1 0 0 0 0 0 0 20 16
    1 30 31.83 24.42 1.20 0 0 0.00 10
    1 50 22.47 17.27 1.50 0 0 0.69 10
    1 0 0.00 0.00 0.00 0 0 0.00 0
    1 0 0.00 0.00 0.00 0 0 0.00 0
    1 0 0.00 0.00 0.00 0 0 0.00 0

- Output 2: csv file with a list of param.sol contained in the zip archive associated to the uts number

    uts,cond_Stics.FSol
    320199,320199.gc.param.sol
    320200,320200.gc.param.sol
    320201,320201.gc.param.sol
    320202,320202.gc.param.sol
    320219,320219.gc.param.sol
    320220,320220.gc.param.sol
    320221,320221.gc.param.sol
    320222,320222.gc.param.sol
    320291,320291.gc.param.sol

- Ouput 3: tempopar file with pebble type

-----

*Author*
  Estelle Ancelet, Inra

*Licence*
  GPLv3

