#!/bin/env python
#Estelle Ancelet
#get data form Infosol datamart

import psycopg2, argparse, pandas, os, shutil

# look for credentials to connect to the database among the environment variables
dbHost = os.environ.get("AGGLOB_SOIL_HOST")
dbName = os.environ.get("AGGLOB_SOIL_DATABASE")
dbPass = os.environ.get("AGGLOB_SOIL_PASSWORD")
dbUser = os.environ.get("AGGLOB_SOIL_USER")

conn = psycopg2.connect(dbname=dbName, user=dbUser, host=dbHost, password=dbPass)

soils = pandas.read_sql("""SELECT pgc.*, 
                            pgc.norg AS norg_gc,
                            psth.norg AS norg_sth 
                            FROM dm_agglob.paramsol_gc pgc
                            JOIN dm_agglob.paramsol_sth psth ON pgc.stu = psth.stu""", 
                        conn, coerce_float=False)
pebble_def = pandas.read_sql("SELECT * FROM dm_agglob.typecailloux ORDER BY typecailloux", conn, coerce_float=False)

parser = argparse.ArgumentParser()
parser.add_argument("-c","--crop_type", required=True, help="gc for field crops and sth for permanent grassland")
parser.add_argument("-s", "--stics_soil_archive", required=True)
parser.add_argument("-l", "--soil_list", required=True)
parser.add_argument("-tt", "--tempopar_template", required=True)
parser.add_argument("-tn", "--tempopar_new", required=True)
args = parser.parse_args()

# create frame which will be converted to csv report
# the report contains the soil file list as expected by cvle 
planColumns = ['uts', 'cond_Stics.FSol']
planData = pandas.DataFrame(index=None, columns=planColumns)

folderDest = "sticsSoilFiles"
os.mkdir(folderDest)
# loop among sol types
for row, sticsUnit in soils.iterrows():
    ucm = str(sticsUnit['stu'])
    fileName = ucm + '.' + args.crop_type + '.param.sol'
    with open(folderDest + "/" + fileName, "w") as soilFile:
        numsol = "1" # numsol set to 1 due to a bug in stics that skips the forcing of P_ichsl
	    # numsol = ucm 
        planData = planData.append(
            {'uts' : ucm,
            'cond_Stics.FSol' : fileName}, 
            ignore_index=True)
        soilFile.write(numsol + " " +
            "sol_" + ucm + "_" + args.crop_type + " " + 
            str(sticsUnit['argi']) + " " +
            str(sticsUnit['norg_' + args.crop_type]) + " " +
            str(sticsUnit['profhum']) + " " +
            str(sticsUnit['calc']) + " " +
            str(sticsUnit['ph']) + " " +
            str(sticsUnit['concseuil']) + " " +
            str(sticsUnit['albedo']) + " " +
            str(sticsUnit['q0']) + " " +
            str(sticsUnit['ruisolnu']) + " " +
            str(sticsUnit['obstarac']) + " " +
            str(sticsUnit['pluiebat']) + " " +
            str(sticsUnit['mulchbat']) + " " +
            str(sticsUnit['zesx']) + " " +
            str(sticsUnit['cfes']) + " " +
            str(sticsUnit['z0solnu']) + " " +
            str(sticsUnit['csurn']) + " " +
            str(sticsUnit['penterui']) + "\n")
        soilFile.write(numsol + " " +
            str(sticsUnit['codecaillou']) + " " +
            str(sticsUnit['codemacropore']) + " " +
            str(sticsUnit['codefente']) + " " +
            str(sticsUnit['codedrain']) + " " +
            str(sticsUnit['coderemcap']) + " " +
            str(sticsUnit['codenitrif']) + " " +
            str(sticsUnit['codedenit']) + "\n")
        soilFile.write(numsol + " " +
            str(sticsUnit['profimper']) + " " +
            str(sticsUnit['ecartdrain']) + " " +
            str(sticsUnit['ksol']) + " " +
            str(sticsUnit['profdrain']) + " " +
            str(sticsUnit['capiljour']) + " " +
            str(sticsUnit['humcapil']) + " " +
            str(sticsUnit['profdenit']) + " " +
            str(sticsUnit['vpotdenit']) + "\n")
        soilFile.write(numsol + " " +
            str(sticsUnit['epc1']) + " " +
            str(sticsUnit['hccf1']) + " " +
            str(sticsUnit['hminf1']) + " " +
            str(sticsUnit['daf1']) + " " +
            str(sticsUnit['cailloux1']) + " " +
            str(sticsUnit['typecailloux1']) + " " +
            str(sticsUnit['infil1']) + " " +
            str(sticsUnit['epd1']) + "\n")
        soilFile.write(numsol + " " +
            str(sticsUnit['epc2']) + " " +
            str(sticsUnit['hccf2']) + " " +
            str(sticsUnit['hminf2']) + " " +
            str(sticsUnit['daf2']) + " " +
            str(sticsUnit['cailloux2']) + " " +
            str(sticsUnit['typecailloux2']) + " " +
            str(sticsUnit['infil2']) + " " +
            str(sticsUnit['epd2']) + "\n")
        soilFile.write(numsol + " " +
            str(sticsUnit['epc3']) + " " +
            str(sticsUnit['hccf3']) + " " +
            str(sticsUnit['hminf3']) + " " +
            str(sticsUnit['daf3']) + " " +
            str(sticsUnit['cailloux3']) + " " +
            str(sticsUnit['typecailloux3']) + " " +
            str(sticsUnit['infil3']) + " " +
            str(sticsUnit['epd3']) + "\n")
        soilFile.write(numsol + " " +
            str(sticsUnit['epc4']) + " " +
            str(sticsUnit['hccf4']) + " " +
            str(sticsUnit['hminf4']) + " " +
            str(sticsUnit['daf4']) + " " +
            str(sticsUnit['cailloux4']) + " " +
            str(sticsUnit['typecailloux4']) + " " +
            str(sticsUnit['infil4']) + " " +
            str(sticsUnit['epd4']) + "\n")
        soilFile.write(numsol + " " +
            str(sticsUnit['epc5']) + " " +
            str(sticsUnit['hccf5']) + " " +
            str(sticsUnit['hminf5']) + " " +
            str(sticsUnit['daf5']) + " " +
            str(sticsUnit['cailloux5']) + " " +
            str(sticsUnit['typecailloux5']) + " " +
            str(sticsUnit['infil5']) + " " +
            str(sticsUnit['epd5']) + "\n")
# make an archive with param.sol files and return it to galaxy
shutil.make_archive('./' + folderDest, 'zip', base_dir=folderDest)
shutil.rmtree(folderDest)
os.rename(folderDest+'.zip', args.stics_soil_archive) 

# write soil list in file
planData.to_csv(args.soil_list, sep=',', header=True, index=False)

# create new tempopar and get it back to galaxy with pebble definition
pebble_count = 10
with open(args.tempopar_template, 'r') as temp_src:
    with open(args.tempopar_new, 'w') as temp_target:
        for line, data in enumerate(temp_src):
            if line < 212 or line > 251: 
                temp_target.write(data)
            elif pebble_count == 10:
                for row, pebble in pebble_def.iterrows():
                    temp_target.write(
                        'masvolcx' + '\n' +
                        str(pebble['dacx']) + "\n" +
                        'hcccx' + '\n' +
                        str(pebble['hcccx']) + "\n")
                    pebble_count -= 1
                if 0 < pebble_count <= 10:
                    for i in range(0, pebble_count):
                        temp_target.write(
                            'masvolcx' + '\n' +
                            '0' + "\n" +
   	                        'hcccx' + '\n' +
                            '0' + "\n")




