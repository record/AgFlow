# author : eancelet
# date : 2018-05-02

import argparse, os
from oauthlib.oauth2 import LegacyApplicationClient
from requests_oauthlib import OAuth2Session
import zipfile, StringIO

parser = argparse.ArgumentParser()
parser.add_argument("-p","--plan", required=True)
args = parser.parse_args()

client_id = '1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4'
client_secret = '4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k'
username = 'record_agglob'
password = '@u89fL0Wi'

oauth = OAuth2Session(client=LegacyApplicationClient(client_id=client_id))
token = oauth.fetch_token(token_url='https://odr.inra.fr/api_token/web/app.php/oauth/v2/token',
        username=username, password=password, client_id=client_id,
        client_secret=client_secret)

#  https://odr.inra.fr/WS/agGlob/index.php # to get info on available data
data = oauth.get('https://odr.inra.fr/WS/agGlob/getData.php?version=usm_odr&annee=2012')

z = zipfile.ZipFile(StringIO.StringIO(data.content))
#z.extractall()
plan_file = 'STICS_USM_GC.csv'
z.extract(plan_file)

os.rename(plan_file, args.plan)
