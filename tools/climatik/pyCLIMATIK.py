#!/usr/bin/env python

from optparse import OptionParser
from datetime import datetime, date, time
from datetime import timedelta

import sys
import osa
import csv, codecs, cStringIO

def createFileDatas(options, cl, ticket, outputFile, variables):

    if outputFile == '':
        outputFile = 'datas.csv'

    with open(outputFile, 'w') as csvfile:
        fieldnames = ['AN',
                      'JOUR',
                      'MOIS']
        for i in range(len(variables)):
            fieldnames.append(variables[i])

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        startDatePeriod = datetime.strptime(options.begin, "%d/%m/%Y").date()
        endDatePeriod = datetime.strptime(options.end, "%d/%m/%Y").date()
        dateInter = startDatePeriod
        dateDebut = startDatePeriod

        while  (endDatePeriod - dateDebut) > timedelta(days = 0) :
            if (endDatePeriod - dateDebut) > timedelta(days = 59) :
                dateInter = dateInter + timedelta(days = 59)
            else :
                dateInter = endDatePeriod

            query = cl.types.getDailyDataByStation1()

            arString = cl.types.ArrayOfString()
            arString.string = options.variables.split(",")

            query.ticket = ticket
            query.startDatePeriod = dateDebut.strftime("%d/%m/%Y")
            query.endDatePeriod = dateInter.strftime("%d/%m/%Y")
            query.stationId = options.station
            query.arrayOfVariables =  arString

            datas =  cl.service.getDailyDataByStation1(query)

            for i in range(len(datas.DailyData)):
                dict = {}
                iDailyData = datas.DailyData[i]
                for j in range(len(fieldnames)):
                    dict[fieldnames[j]] = getattr(iDailyData, fieldnames[j])

                writer.writerow(dict)

            dateDebut = dateInter + timedelta(days = 1)


def createFileStations(stations, outputFile):

    if outputFile == '':
        outputFile = 'stations.csv'

    with open(outputFile, 'w') as csvfile:
        fieldnames = ['altitude',
                      'chgDate',
                      'city',
                      'coefv',
                      'ddLatitude',
                      'ddLongitude',
                      'endDate',
                      'id',
                      'interrogationType',
                      'latitude',
                      'longitude',
                      'organism',
                      'place',
                      'startDate',
                      'stationType']

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        stationsIter = iter(stations.StationData)

        for i in range(len(stations.StationData)):
            #print stations.StationData[i].altitude
            iStation = stations.StationData[i]
            writer.writerow({"altitude":iStation.altitude,
                             "chgDate":iStation.chgDate,
                             "city":iStation.city,
                             "coefv":iStation.coefv,
                             "ddLatitude":iStation.ddLatitude,
                             "ddLongitude":iStation.ddLongitude,
                             "endDate":iStation.endDate,
                             "id":iStation.id,
                             "interrogationType":iStation.interrogationType,
                             "latitude":iStation.latitude,
                             "longitude":iStation.longitude,
                             "organism":iStation.organism,
                             "place":iStation.place.encode('utf-8').strip(),
                             "startDate":iStation.startDate,
                             "stationType":iStation.stationType})

def createFileVariables(variables, outputFile):

    if outputFile == '':
        outputFile = 'variables.csv'

    with open(outputFile, 'w') as csvfile:
        fieldnames = ['complement1',
                      'complement2',
                      'complement3',
                      'complement4',
                      'complement5',
                      'complement6',
                      'fullName',
                      'shortlabel',
                      'type',
                      'unit']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for i in range(len(variables.VariableData)):
            iVariable = variables.VariableData[i]
            writer.writerow({"complement1":iVariable.complement1.encode('utf-8').strip(),
                             "complement2":iVariable.complement2.encode('utf-8').strip(),
                             "complement3":iVariable.complement3.encode('utf-8').strip(),
                             "complement4":iVariable.complement4.encode('utf-8').strip(),
                             "complement5":iVariable.complement5.encode('utf-8').strip(),
                             "complement6":iVariable.complement6.encode('utf-8').strip(),
                             "fullName":iVariable.fullName,
                             "shortlabel":iVariable.shortlabel,
                             "type":iVariable.type,
                             "unit":iVariable.unit})

def main(argv):
    parser = OptionParser()
    parser.add_option("-u", "--user", dest="user",
                      help="user name")
    parser.add_option("-p", "--password", dest="password",
                      help="user password")
    parser.add_option("-f", "--file", dest="filename",
                      help="write report to FILE", metavar="FILE")
    parser.add_option("-v", "--variables", dest="variables",
                      help="list of variables, example : -v RG,Rain")
    parser.add_option("-c", "--command", dest="command",
                      help="command are variables|stations|datas")
    parser.add_option("-s", "--station", dest="station",
                      help="station id, example: -s 78615002")
    parser.add_option("-b", "--begin", dest="begin",
                      help="begin date, example: -b 01/02/2010")
    parser.add_option("-e", "--end", dest="end",
                      help="end date, example: -e 31/03/2010")

    parser.add_option("-q", "--quiet",
                      action="store_false", dest="verbose", default=True,
                      help="don't print status messages to stdout")

    (options, args) = parser.parse_args()

    cl = osa.Client(" https://intranet.inra.fr/climatik_v2/ws/xfire/AlternateWebService?wsdl")
    ticket = cl.service.connexion(options.user, options.password)

    if options.command == "stations" :
        stations = cl.service.getStations(ticket)
        createFileStations(stations, options.filename)
    elif options.command == "variables" :
        variables = cl.service.getVariables(ticket)
        createFileVariables(variables, options.filename)
    elif options.command == "datas" :
        createFileDatas(options, cl, ticket, options.filename, options.variables.split(","))

if __name__ == "__main__":
   main(sys.argv[1:])
