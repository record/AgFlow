echo "preStics4AgGlob Job starting:"
date;

csv_plan=$1

cp $2 tempopar.sti

folderSoil="soil"
folderPlt="plt"
folderItk="itk"

unzip $3
unzip -j $5 -d $folderPlt
unzip -j $6 -d $folderSoil
unzip -j $7 -d $folderItk

folderMeteo="meteo"
unzip -j $4 -d $folderMeteo

workFold="RECUP"

Rscript "$8/convert_usms_allsteps.R" $csv_plan $folderSoil $folderMeteo $folderItk $folderPlt $8 $workFold 2>&1

cp inputs_usms_filtered_completed.csv $9

echo "preStics4AgGlob Job ending:"
date;
