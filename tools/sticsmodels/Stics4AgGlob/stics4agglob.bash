echo "Stics4AgGlob Job starting:"
date;

n_slots=${1};
csv_plan=${2};
sim_number=$(($(cat ${csv_plan} | wc -l) - 1));

b_size=1

echo "number of GALAXY_SLOTS: ${1}"

echo "localhost slots="${1} > ./hostfile

echo "expected simulations: ${sim_number}"
echo "cvle block size : ${b_size}"

cp ${3} tempopar.sti
cp ${4} tempoparv6.sti
cp ${5} s2280.txt
cp ${6} climat.txt

echo "Unzip crop, soil and technical management archives..."
folderSoil="soil"
folderPlt="plt"
folderItk="itk"

unzip ${7}

unzip -j ${8} -d $folderPlt
unzip -j ${9} -d $folderSoil
unzip -j ${10} -d $folderItk

folderMeteo="meteo"

unzip -j ${11} -d $folderMeteo

mkdir RECUP

mpirun --allow-run-as-root --hostfile hostfile -d -v -np ${1} cvle --withoutspawn -b $b_size -i ${2} -P Stics4AgFlow stics_rotation_cvle_full_option.vpz 2>&1

fileSuffix="Outputs.txt"

zip output *$fileSuffix
mv output.zip ${12}

echo "Stics4AgGlob Job ending:"
date;
