The folder where the Stics4AgGlob container is maintained.
And where pre and pos tretement are provided also.

To build, push, and use the container:

To build:
`> docker build -t chabrier/stics4agglob container`

To push:
`> docker push chabrier/stics4agglob:latest`

To use cvle with a simulator close yo the one dedicated to AgGlob,
If everything is set inside your local folder(see pre)
`> singularity exec docker://chabrier/stics4agglob:latest mpirun --hostfile hostfile -d -v -np 4 cvle -o outFile --withoutspawn -b 10 -i simuOne.csv -P Stics4AgFlow stics_rotation_cvle_full_option_1simu.vpz`

to keep in mind a command line running pretreatment
`> ../preStics4AgGlob.sh ../test-dataPre/ODR-plan.csv ../test-dataPre/tempopar.sti ../test-dataPre/stics.zip ../test-dataPre/weather_safran.zip ../test-dataPre/plt.zip ../test-dataPre/param_sol_archive.zip ../test-dataPre/itk.zip ../R`

to use the container stics4agglob:
```
> mkdir working
> cd working
> singularity exec docker://chabrier/stics4agglob:latest bash ../stics4agglob.bash 4 ../test-data/cvle-Stics4AgGlob-dummy-plan.csv ../test-data/tempopar.sti ../test-data/tempoparv6.sti ../test-data/s2280.txt ../test-data/climat.txt ../test-data/stics.zip ../test-data/crop-dummy.zip ../test-data/soil-dummy.zip ../test-data/itk-dummy.zip ../test-data/weather-dummy.zip resultat.zip
```
