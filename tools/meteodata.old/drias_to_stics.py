# !/bin/env python
# Estelle Ancelet
# parse DRIAS files and build Stics Climat Files


import pandas
import argparse
import os
import shutil
import zipfile
import tarfile
import re
import math

##################################################################################################################################################
#
# FUNCTIONS
#
##################################################################################################################################################


def tmean(t1,t2) :
    """
    @summary: return the average between 2 values
    """
    return (t1 + t2)/2 

def driasToStics( srcFile, srcName, fileFormat, cellId ) :
    """
    @summary: takes a drias file, creates the stics formatted file and get back the path to the file
    """
    
    # define a file where the data are temporarly stored (as a zipfile.write accept only files)
    tempoFile = 'tempofile.txt'
        
    # get header names from file metadata
    lineFormat = None
    while not lineFormat:
        line = srcFile.readline()
        if line == '# Format de la ligne :\n':
            lineFormat = srcFile.readline()
    lineFormat = lineFormat.replace('#','')
    lineFormat = lineFormat.replace('"','')
    colNames = lineFormat.split()
   
    # handle dates
    colNames.remove('Date')
    colNames.insert(0,'yyyy')
    colNames.insert(1,'mm')
    colNames.insert(2,'dd')    
    
    # TODO : eancelet 2018-05-02, add test on cnames to be sure we have what we need
    # Verify file format
    isFileOk = True

    if isFileOk:
        print 'Weather file : ' + srcName + ' -> structure ok. Parsing file...'

        # File parsing
        # comment -> skip commented lines 
        # index_col=False -> use row number as index
        # header=None -> use column number index
        climatData = pandas.read_csv(srcFile, comment='#', index_col=False, sep=';', header=None, names=colNames)
    
        # add missing columns
        climatData['co2'] = ['330.0'] * len(climatData)
        climatData['cellId'] = [cellId] * len(climatData)
        # unit conversions
        climatData['globalRad'] = climatData.apply(lambda row: row['rsds'] * 0.0864, axis='columns')
        
        if fileFormat == 'generic_with_header':                
            # TODO : add PET calculation
            climatData['ETP'] = climatData.apply(lambda row: 0.035 * row['globalRad'] * ((row['tasmin']+row['tasmax'])/2 + 17.8) * 0.408, axis='columns')
            # rename columns
            climatData = climatData.rename(columns={'cellId': 'ID_point', 'yyyy': 'Annee', 'mm': 'Mois', 'dd': 'Jour', 
                                                    'tasmin': 'Tmin', 'tasmax': 'Tmax', 
                                                    'rstr': 'Rain',
                                                    'globalRad': 'RG', 'co2': 'Co2'})            
            # column reordering and filtering
            climatOutputData = climatData.filter(['ID_point','Annee','Mois','Jour','Tmin','Tmax','Rain','ETP','RG','Co2']) 
            print '    Write weather file...'
            climatOutputData.to_csv(tempoFile, sep=';', header=True, index=False, decimal='.')
			        
        else: 
            # add missing columns used by stics model directly
            climatData['penmanPet'] = [-999.9] * len(climatData) # in the stationfile, codeetp is set to 2
            climatData['wind999'] = [-999.9] * len(climatData)
            climatData['vapourPressure'] = climatData.apply(
                lambda row: math.pow(10, 2.7862+(7.5526 * tmean(row['tasmin'], row['tasmax']) / (239.21 + tmean(row['tasmin'], row['tasmax'])))) 
                if tmean(row['tasmin'], row['tasmax']) >= 0 
                else math.pow(10, 2.7862+(9.7561 * tmean(row['tasmin'], row['tasmax']) / (272.67 + tmean(row['tasmin'] + row['tasmax'])))), 
                axis='columns')
            climatData['julianDay'] = range(1, len(climatData) +1, 1)         
            # column reordering and filtering
            climatOutputData = climatData.filter(['cellId','yyyy','mm','dd','julianDay','tasmin','tasmax','globalRad','penmanPet','rstr','wind999','vapourPressure','co2']) 
            print '    Write weather file...'
            climatOutputData.to_csv(tempoFile, sep=' ', header=False, index=False, decimal='.')

    else :
        print 'Climat file : ' + fileSrc + ' -> format does not have the attented structure (Date tasmin tasmax rstr huss rsds rlds sfcwind).'

    return tempoFile



##################################################################################################################################################
#
# PROCESS
#
##################################################################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument("-c","--climat", required=True, help="Your DRIAS archive (zip)")
parser.add_argument("-s","--codification", required=True)
parser.add_argument("-f","--weather_format", required=True)
parser.add_argument("-l","--weather_file_list", required=True, help="The name of the cvlePlan generated by this script")
parser.add_argument("-a","--archive", required=True, help="The name of the archive containing the stics meteo file, generated by this script (must be a tar.gz)")
args = parser.parse_args()


#with zipfile.ZipFile(args.climat) as climatZip:
#    for member in climatZip.namelist():
#        filename = os.path.basename(member)
#        # skip directories
#        if not filename:
#            continue
#        # copy file (taken from zipfile's extract)
#        source = climatZip.open(member)
#        target = file(os.path.join(folderSrc, filename), "wb")
#        with source, target:
#            shutil.copyfileobj(source, target)
#    climatZip.close()

targetArchive = zipfile.ZipFile(args.archive,"w",zipfile.ZIP_DEFLATED,allowZip64=True)

# load codification match if needed
if args.codification == 'safran':
    cMatching = ['drias_id','safran_id']
    cFile = '/work/galaxy-vle/my_tools/meteodata/2018-04-20_safran_drias_match.csv'
    cData = pandas.read_csv(cFile, index_col=0, sep=';')

# creating weather file list
planColumns = ['cell_id','cond_Stics.FClimat']
planData = pandas.DataFrame(index=None, columns=planColumns)

temporaryFileName = None
with zipfile.ZipFile(args.climat) as climatZip:
    print 'Creating climat files archive...'
    for member in climatZip.namelist():
        srcFileName = os.path.basename(member)
        # skip directories
        if not srcFileName:
            continue
        source = climatZip.open(member)  
        
        # get id from file name
        # if needed, switch to safran id
        if args.codification == 'safran':
            cellId = re.sub("^0+","",srcFileName[1:6])
            cellId = cData.loc[int(cellId)].maille_safran        
            climatFileName = 'safran_' + str(cellId) + '.txt'
        else:
            cellId = srcFileName[1:6]
            climatFileName = 'drias_' + cellId + '.txt'        
#           climatOutPath = folderDest + '/' + climatFile

        temporaryFileName = driasToStics(source, srcFileName, args.weather_format, cellId)      
        targetArchive.write(temporaryFileName,'sticsClimatFiles/' + climatFileName)
                
        print '    Add weather file to list...'  
        planData = planData.append({'cell_id' : cellId,
                                    'cond_Stics.FClimat' : climatFileName}, 
                                    ignore_index=True)

    climatZip.close()

if os.path.isfile(temporaryFileName):
    os.remove(temporaryFileName)
targetArchive.close()
print 'Archive created'

print 'Creating weather file list .csv...'
planData.to_csv(args.weather_file_list, sep=',', header=True, index=False)
