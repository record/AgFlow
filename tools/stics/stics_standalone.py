#!/bin/env python
#Estelle Ancelet
#run a stics model without inputs and one output

import os, sys, pyvle, argparse, zipfile
from pyvle import Vle  

import shutil, uuid, getpass

# vle simulation 
dir(pyvle.Vle)  
exp = Vle("stics_param_file.vpz", "Stics4AgFlow")  

parser = argparse.ArgumentParser()
parser.add_argument("-o1", "--output1", required=True, help="Directs the output to a name of your choice 1")
parser.add_argument("-c","--climat", help="Your climat file (txt)")
parser.add_argument("-u","--usm", help="Zip archive containing the remaining usm files")
args = parser.parse_args()

# args.climat : if the script is launched by command line
# args.climat!='None' : if Galaxy launches the script. Then -c is mapped with 'None'
if args.climat and args.climat!='None':
    #exp.setConditionPortValue('cond_Stics', 'FClimat', '../../../../../../..' + args.climat, 0)
    exp.setConditionPortValue('cond_Stics', 'FClimat', args.climat, 0)
else: 
    print 'Pas de fichier climat fourni'



# idem with usm data
# TODO : create function?
# TODO : check the folder number? So far, we assume that all the usm data are in one folder in the archive
# TODO : add input security checks?
if args.usm and args.usm!='None':
    usmZip = zipfile.ZipFile(args.usm, 'r') 
    usmFold = usmZip.namelist()[0]
    usmZip.extractall()
    exp.setConditionPortValue('cond_Stics', 'SticsInputDir', usmFold, 0)
else: 
    print "Pas d'archive usm fournie"

exp.run() 

# mv ouput file so galaxy returns the file we want
os.rename('tutorial_stics_vue_stics.csv',args.output1)

