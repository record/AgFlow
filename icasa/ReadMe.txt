File list :
	- ble.txt, mais.txt : expected files in STICS format (fictec)
	- template_ble.csv, tempate_mais.csv : files in ICASA format obtained from the template (filled by agronomists)  
	- matching_icasa_to_stics_V1 : matching between the STICS variables and the ICASA variables. The file begins with the list of all the STICS variables used for management.The matching begins in the latter part of the file.
	-template.ods :  spreadsheet to be filled by the user
	- icasa_to_stics.py : convert from ICASA format to STICS format and generate a file containing values of the technical file
	- comparison.py : compare two files in STICS format and generate a file with the encountered differences.
	- test_icasa_to_stics.py : test the icasa_to_stics module. 
