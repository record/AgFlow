#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 13:52:22 2018

@author: chlatorre

How to run this script : 
  python test_icasa_to_stics.py 
"""

import unittest
import logging
import subprocess
import icasa_to_stics as cs
import comparison as comp

class SticsTest(unittest.TestCase):
    
    def test_csv_not_found(self):
        logging.getLogger()
        self.assertRaises(IOError,cs.crea_data_array,"nonexistingfile.csv")
    
    def test_convert_code_residue(self):
        self.assertEqual(7, cs.convert_code_residue('RE203'))
        
    def test_convert_code_fertilizer(self):
        self.assertEqual(7, cs.convert_code_fertilizer('FE008'))
        
    def test_fail_convert_fertilizer(self):
        self.assertEqual(None, cs.convert_code_fertilizer('FE'))
         
    def test_convert_date(self):
        jd,verif_s_date = cs.convert_date('1989/02/28','1989/02/12')
        self.assertEqual(59, jd)
        self.assertEqual(verif_s_date,True)
        
    def test_fail_convert_date(self):
        jd,verif_s_date = cs.convert_date('1989/02/28','1980/02/12')
        self.assertEqual(verif_s_date,False)
        
    def test_date_min(self):
        dic = dict()
        dic = {"date_PL": ["1999/09/05"],
               "date_OM" : ["1999/02/21","1999/02/12"],
               "a" : ["1999/01/01"]} 
        date_min, var_min = cs.date_min(dic)
        self.assertEqual('1999/02/12',date_min)
        self.assertEqual('date_OM',var_min)
        
    def test_conversion(self):
        subprocess.call('icasa_to_stics.py -i template_ble.csv -f fictec_ble.txt -l log_ble.txt', shell=True)
        #cs.main()
        diff_file = "diff_ble.txt"
        comp.file_comparison("fictec_ble.txt","ble.txt",diff_file)
        diff = open(diff_file, "r")
        list_diff = diff.readlines()
        expected_list = ['variete\r\n','1\r\n','2\r\n','iamf\r\n','999\r\n','442\r\n','irecbutoir\r\n','300\r\n',
                         '700\r\n','profmes\r\n','120.00000\r\n','60.00000\r\n','h2ograinmax\r\n',
                         '0.32000\r\n','0.14000\r\n']
        self.assertEqual(list_diff,expected_list)
        diff.close()
        
if __name__ == '__main__':
    unittest.main()


