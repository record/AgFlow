# -*- coding: utf-8 -*-
"""
How to run this script : 
  python icasa_to_stics.py -i itk_csv -f fictec_txt -l log_txt
    
"""


# To obtain the same result for divisions with Python 2 and Python 3 :
from __future__ import division 
# To create logs :
import logging
# To convert dates :
from datetime import datetime, date 
# To create a dataframe :
import pandas as pa
# To use regular expressions : 
import re 
# To test file accessibility :
import os
import argparse




def crea_log(log_file) :
    """
    Create a file and a stream handlers.
    :param log_file: log file
    :type log_file: string
    """    
    
    # Set level to DEBUG
    logger.setLevel(logging.DEBUG)
     
    # Create formatter
    formatter = logging.Formatter("%(asctime)s -- %(name)s -- %(levelname)s --"
                                  "%(message)s")
    
    # Create handler rederecting messages towards a disk file.
    file_handler = logging.FileHandler(log_file, 'w')
    # Set level to DEBUG and use the specified format
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    # Add this handler to logger
    logger.addHandler(file_handler)
     
    # Create another handler redirecting messages into the console
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    logger.addHandler(stream_handler)
    
    
    
def close_log() :
    " Close the file and the stream handlers." 
    
    x = list(logger.handlers)
    for i in x:
        logger.removeHandler(i)
        i.flush()
        i.close()



def crea_stics_dict_def():
    """
        Create a dictionary with the default values for the non mandatory
        variables.
        :return: dictionary with the default values for the non mandatory 
        variables.
        :rtype: dictionary
    """
    
    # Create the entries for the variables appearing only once. 
    dic_def ={'densitesem': 9.5, 'variete': 1, 'codetradtec': 2, 
              'interrang':0.0, 'orientrang':0.0,
              'codedecisemis':2,'nbjmaxapressemis':30,
              'nbjseuiltempref':15, 'codestade': 2, 'ilev' : 999,
              'iamf': 999,'ilax':999,'isen':999,'ilan':999,'iflo':999,
              'idrp':999,'imat':999,'irec':999, 'irecbutoir': 300,
              'effirr':1.0, 'codecalirrig' :1, 'ratiol' :0.0,
              'dosimx':40.0, 'doseirrigmin':20.0,'codedateappH2O':2,
              'codlocirrig':1, 'locirrig': 0, 'profmes':120.0,
              'engrais':2, 'concirr': 0.11,'codedateappN':2,
              'codefracappN':1,'Qtot_N':100, 'codlocferti':1,
              'locferti':0, 'ressuite': 'straw +roots','codceuille':1,
              'nbceuille':1,'cadencerec':7,'codrecolte':1,'codeaumin':2,
              'h2ograinmin': 0.80, 'h2ograinmax' : 0.32,
              'sucrerec' : 0.25, 'CNgrainrec' : 0.02, 'huilerec':0.25,
              'coderecolteassoc': 2, 'codedecirecolte' : 2,
              'nbjmaxapresrecolte' : 15, 'codefauche' : 2, 
              'mscoupemini' : 0.0, 'codemodfauche' : 1, 
              'hautcoupedefaut' : 0.0, 'stadecoupedf': 'rec', 
              'codepaillage' : 1, 'couvermulchplastique':0.0,
              'albedomulchplastique': 0.0, 'codrognage':1,
              'largrogne': 0.0, 'hautrogne':0.0, 'biorognem': 0.0, 
              'codcalrogne': 1, 'julrogne': 0, 'margerogne' : 0.0,
              'codeclaircie':1, 'juleclair': 0, 'nbinfloecl' : 0.0,
              'codeffeuil' : 1, 'codhauteff' : 1, 'codcaleffeuil': 1,
              'laidebeff': 0.0, 'effeuil' : 0.0, 'juleffeuil':0,
              'laieffeuil': 0.0, 'codetaille': 1, 'jultaille': 0,
              'codepalissage' : 1, 'hautmaxtec' : 0.0, 'largtec' : 0.5,
              'codabri' : 1, 'transplastic':0.7, 'surfouvre1' : 0.0, 
              'julouvre2': 0, 'surfouvre2': 0.0, 'julouvre3': 0, 
              'surfouvre3' : 0.0, 'codeDST': 2, 'dachisel' : 1.1, 
              'dalabour' : 1.3, 'rugochisel' : 0.005, 'rugolabour':0.05,
              'codeDSTtass': 2, 'profhumsemoir' : 30.0, 'dasemis':1.6,
              'profhumrecolteuse' : 30.0, 'darecolte':1.6, 
              'codeDSTnbcouche' : 2}
   
    # Create the entries for the variables that can appeared between 0 to n times.
    dic_def.update({'julres':999 ,'coderes': 1 ,'qres': 1.0 ,'Crespc': 42.0 ,
                    'CsurNres': 60.0, 'Nminres':0.0 ,'eaures':0.0 ,
                    'jultrav': 112, 'profres': 0.0 ,'proftrav': 25.0, 'amount': 0,
                    'julapN_or_sum_upvt':112,'absolute_value':220})
    
    return dic_def

def crea_stics_list_unique_var() : 
    """ 
        Create : - the list of variables appearing only once per category ;
                 - the list of variables appearing only once.
        :return: list of the variables appearing only once in the technical
        file.
        :rtype: list            
    """ 
                 
    amendment = ['nb_amen']
    tillage = ['nb_till']
    sowing = ['iplt0','profsem','densitesem','variete','codetradtec',
              'interrang', 'orientrang','codedecisemis','nbjmaxapressemis',
              'nbjseuiltempref']
    phenology = ['codestade','ilev','iamf','ilax','isen','ilan','iflo','idrp',
                 'imat','irec','irecbutoir']
    irrigation =['effirr','codecalirrig','ratiol','dosimx','doseirrigmin',
                 'codedateappH2O','nb_irri','codlocirrig','locirrig','profmes']
    fertilization =['engrais','concirr','codedateappN','codefracappN','Qtot_N',
                    'nb_fert', 'codlocferti','locferti','ressuite']
    harvest=['codceuille','nbceuille','cadencerec','codrecolte','codeaumin',
             'h2ograinmin','h2ograinmax','sucrerec','CNgrainrec', 'huilerec',
             'coderecolteassoc', 'codedecirecolte', 'nbjmaxapresrecolte']
    special_operations = [ 'codefauche', 'mscoupemini', 'codemodfauche', 
              'hautcoupedefaut', 'stadecoupedf','nb_cutt_days',
              'nb_cutt_degree_days', 'codepaillage','couvermulchplastique', 
              'albedomulchplastique', 'codrognage', 'largrogne', 
              'hautrogne', 'biorognem', 'codcalrogne', 'julrogne',
              'margerogne', 'codeclaircie', 'juleclair', 'nbinfloecl',
              'codeffeuil', 'codhauteff', 'codcaleffeuil', 'laidebeff',
              'effeuil', 'juleffeuil', 'laieffeuil', 'codetaille', 'jultaille',
              'codepalissage', 'hautmaxtec', 'largtec', 'codabri', 
              'transplastic', 'surfouvre1', 'julouvre2', 'surfouvre2', 
              'julouvre3', 'surfouvre3', 'codeDST', 'dachisel', 'dalabour',
              'rugochisel', 'rugolabour', 'codeDSTtass', 'profhumsemoir',
              'dasemis', 'profhumrecolteuse', 'darecolte', 'codeDSTnbcouche']
        
    list_unique_variables = amendment + tillage + sowing + phenology + irrigation + \
            fertilization + harvest + special_operations
    return list_unique_variables



def crea_stics_list_opp() :
    """ 
        Create : - the list of the variable counting the number of 
                   interventions of each type ;
                 - the variables list that can be repeated for each type of 
                   intervention;
                 - the list of all the  types of intervention;
        :return nb_int: variables that will be named 'nbinterventions' in the 
        technical file.
        :return opp: list of the operations that can be performed several times. 
        :rtype nb_int: list
        :rtype opp: list   
    """  
                 
    nb_int = ['nb_amen','nb_till','nb_irri','nb_fert',
              'nb_int_days','nb_int_degree_days']
    opp_amen = ['julres','coderes','qres','Crespc','CsurNres',
                 'Nminres','eaures'] 
    opp_till = ['jultrav','profres','proftrav']
    opp_irri = ['julapI_or_sumupvt','amount']
    opp_fert = ['julapN_or_sum_upvt','absolute_value']
    opp_cutt_days = ['julfauche','hautcoupedefaut','lairesiduel','msresiduel',
                     'anticoupe']
    opp_cutt_degree_days = ['tempsfauche','hautcoupe','lairesiduel',
                            'msresiduel','anticoupe'] 
    opp = [opp_amen, opp_till, opp_irri, opp_fert, opp_cutt_days, 
           opp_cutt_degree_days]
    return nb_int, opp



def crea_data_array(CSV_file):
    """
        Create dataframe from CSV file.
        :param CSV_file: csv file created from the filled template
        :type CSV_file: string
        :return: dataframe created from the csv file 
                 The columns names are the ones of the variables
                 from the template 
    """
    
    if os.path.isfile(CSV_file) :
        logger.info('The file {} exists.'.format(CSV_file))
    else :
        logger.info("The file {} doesn't exist.".format(CSV_file))
    df = pa.read_csv(CSV_file)
    df = df.drop(['Management  data required by STICS','description',
                  'format or unit'],axis=1)
    df = df.drop([0])
    df = df.set_index("variable")
    df.columns = [str(x) for x in range (1,len(df.columns)+1)] # Check if this line is useful.
    df = df.transpose()
    
    return df



def leap_year(y):
    """
        Check if the year is a leap year.
        :param y: the year
        :type y: integer 
        :return: True if y is a leap year, False otherwise 
        :rtype: boolean
    """
    
    leap = True
    if (y % 4)==0:
        if ((y % 100)==0) and ((y % 400)<>0):
            leap = False
    else: leap = False
    return leap
 
    

def verif_date(S):
    """
        Check if a date is correctly entered.
        If this is the case and the year is 1 or 2, write the date 
        in the format YYYY/MM/DD.
        :param S: the date as entered by the user
        :type S: string
        :return date_OK: True if the date format is correct, False otherwise 
        :return S: date in the format YYYY/MM/DD
        :rtype date_OK: boolean
        :rtype S: string
    """
    # Check if the string is containing 2 separators ('/').
    date_OK = True
    L = S.split('/')
    if len(L)!=3:
        logger.error("The date format for is wrong. It must" 
                     " be yyyy/mm/dd. If the year doesn't need to be" 
                     " specified, enter 1 or 2.")
        date_OK = False
    
    # Check if y, m, d are integers.
    try:
        y, m, d = int(L[0]), int(L[1]), int(L[2])
    except:
        logger.error("The date format is wrong. It must be yyyy/mm/dd. If the" 
                     " year doesn't need to be specified, enter 1 or 2.")
        date_OK =  False
                
    # If the format is right, check if the string is matching a date.
    valid_y = (y>=1800 and y<=2100) or (y == 1) or(y == 2)
    if not valid_y :  
        logger.error("The year must be between 1800 et 2100. If it doesn't"
                     " need to be specified, enter 1 or 2.")
        date_OK = False
    if m<1 or m>12 :
        logger.error ('The month must be between 1 and 12. The date format is '
                      'yyyy/mm/dd.')
        
        date_OK = False
    if d<1 or d>(31,29,31,30,31,30,31,31,30,31,30,31)[m-1]:
        logger.error("The day entered can't exist.")
        date_OK = False
    if (not leap_year(y)) and m==2 and d>28:
        logger.error("You didn't enter a leap year, so February can have"
                     "no more than 28 days.")
        date_OK = False
    # If we're here, date is OK.
    if y== 1 :
        S = '0001/'+str(m)+'/'+str(d)
    if y== 2:
        S = '0002/'+str(m)+'/'+str(d)
    return date_OK, S

def verif_type_data(dic_temp):
    """ 
        Check if the type and the format of the data entered is correct.
        For integers and float, convert to the right format in the temporary 
        dictionary.
        :param dic_temp: the dictionary obtained from the dataframe
        :type dic_temp: dictionary
        :return dic_temp: the dictionary entered with some data types amended
        to match the expected types
        :return verif_t: True if the entered data matchs the expected type, 
        False otherwise
        :rtype dic_temp: dictionary
        :rtype verif_t: boolean
    """
        
    verif_t = True
    
    # Check if the values for the decimal variables are properly entered. 
    data_float = ['OMAMT','OMCPC','OMC2N','OMNPC','OMH2O','STICS_profres',
                  'TIDEP','PLDP','PLPOP','IREFF','STICS_concirr',
                  'STICS_absolute_value', 'IRVAL']
    for x in data_float :
        try :
            df[x] = df[x].str.replace(',','.').astype('float64')
            dic_temp[x] = df[x].tolist()
        except :
            verif_t = False
            logger.error("Error(s) when entering value(s) for the variable {}."
                         " These values must be decimals numbers.".format(x))
            
    # Check if the date values are properly entered.  
    data_date = []  
    for x in list(df) : 
        if re.match("date_+", x) :
            data_date.append(x)
           
    for var_name in data_date :
        for i in range(len(dic_temp[var_name])):
            if isinstance(dic_temp[var_name][i],str) : 
                try : 
                    verif_d, dic_temp[var_name][i] = verif_date(
                                                     dic_temp[var_name][i])
                    assert verif_d == True
                    
                except :
                    verif_t = False
                    logger.error("Error(s) when entering value(s) for the"
                                 " variable {}.\n".format(var_name))
       
    # Check if the values of the integers variables are properly
    # entered. If so, convert data to the appropriate format in dic_temp.            
    data_int =  ['nb_OM','TI_NO','IR_no','FEN_no','FEAMN','FEDEP']
    
    for x in data_int :
        for i in range(len(dic_temp[x])):
            if not isinstance(dic_temp[x][i],int):
                if isinstance(dic_temp[x][i],str):
                    try : 
                        dic_temp[x][i] = int(dic_temp[x][i])
                    except :
                        verif_t = False
                        logger.error('Values are not properly entered for the'
                                     ' variable {}. They must be'
                                     ' integers.'.format(x))
                              
    return dic_temp, verif_t

def verif_number_of_data(dic_temp):
    """ 
        Check if the mandatory values are entered and if the number of values  
        entered for the variables describing an operation is consistent with 
        the number of operations of this kind.
        :param dic_temp: the dictionary obtained from the dataframe with the 
        values already converted with the right data type. 
        :type dic_temp: dictionary
        :return: True if the mandatory values are entered and if the number
        of entered values is consistent.
        False otherwise
        :rtype verif_t: boolean
        
    """      
    
    verif_n = True
      
    # Check if the mandatory values which must appeared only once are entered.
    req_amen = ['nb_OM']
    req_till = ['TI_NO']
    req_plant = ['date_PL','PLDP','PLPOP']
    req_fert = ['FEN_no']
    req = req_amen + req_till + req_plant + req_fert
    for x in req :
        if df[x][0:1].count() != 1 :
            verif_n = False
            logger.error('A value must be entered for the variable' 
                         ' {}'.format(x))
              
    if pa.isnull(dic_temp['IR_no'][0]):
        dic_temp['IR_no'][0]=0  
        
    # If the number of interventions of one kind is not 0, some of the 
    # corresponding variables are mandatory.
    l = [['TI_NO',['date_TI','TIDEP']],
         ['IR_no',['date_IR','IRVAL']],
         ['FEN_no',['date_FE','STICS_absolute_value']]]
                
    for el in l :
        for var in el[1]:
            if dic_temp[el[0]][0]!= df[var][0:].count():
                verif_n = False
                logger.error('The number of values entered for the variable {}'
                             ' must be equal to {}, here {}'\
                             .format(var, el[0], dic_temp[el[0]][0]))   
                    
    # For the variables belonging to an operation type, some are never
    # mandatory. 
    l = [['nb_OM',['date_OM','OMCD','OMAMT','OMCPC','OMC2N','OMNPC','OMH2O']],
               ['TI_NO',['STICS_profres']]]
    
    for el in l :
        for var in el[1]:
            if dic_temp[el[0]][0] < df[var][0:].count():
                verif_n = False
                logger.error('Too much values entered for the variable {}.'
                             ' This number of values cannot be greater than' 
                             ' {}, here {}'\
                             .format(var, el[0], dic_temp[el[0]][0])) 
                          
    return verif_n

def crea_dic_template(df) :
    """
        Create dictionary using template and check the type of the entered data.
        :param df: dataframe created from the CSV file
        :type df: dataframe
        :return dic_temp: the dictionary obtained from the dataframe with the 
        values converted with the right data type. 
        :rtype: dictionary
            
    """
    
    dic_temp = dict()
    label_col = list(df)
    for x in label_col :
        dic_temp[x] = df[x].tolist()
    if "TI_NO" not in dic_temp :
        dic_temp["TI_NO"] = dic_temp["Tl_NO"]
        
    for var in ['OMCD','IAME','FECD','STICS_codlocferti','STICS_codrecolte'] :
        if not pa.isnull(dic_temp[var][0]):
            ind = dic_temp[var][0].find(":")
            dic_temp[var][0]= dic_temp[var][0][ind+2:]
    
    dic_temp,verif_type = verif_type_data(dic_temp)
    try : 
        assert verif_type == True
    except : 
        raise Exception("Some values are not valid.")
        
    try : 
        assert verif_number_of_data(dic_temp) == True
    except :
        pass
        raise Exception("Incorrect number of values entered.")
    
    return dic_temp
    

def date_min(dic_temp):
    """
        Find : - the date of the first operation performed;
               - the corresponding variable.
        :param dic_temp: the dictionary obtained from the dataframe with the 
        values converted with the right data type. 
        :type dic_temp: dictionary
        :return date_min_str: date of the first operation performed
        :return var_min_name: corresponding variable name
        :rtype date_min_str: string
        :rtype var_min_name: string   
    """
    date_min_str = dic_temp['date_PL'][0]
    date_mini = datetime.strptime(dic_temp['date_PL'][0], "%Y/%m/%d")
    var_min_name = 'date_PL'
    data_date = []  
    for x in dic_temp : 
            if re.match("date_+", x) :
                data_date.append(x)
               
    for var_name in data_date :
        for el in dic_temp[var_name]:
            
            if isinstance(el,str) :
                date_el = datetime.strptime(el, "%Y/%m/%d")
                if date_el<date_mini :
                    date_min_str = el
                    date_mini = date_el
                    var_min_name = var_name
    
    return date_min_str, var_min_name
            


def convert_date(s,date_mini) :
    """
        Check if the date to convert belong to the same year or to the year 
        just after the first operation performed.
        If so, convert the date in julian days.
        :param s: the date to convert
        :param date_mini: the date of the first operation performed
        :type s: string
        :type date_mini: string
        :return jd: date in julian days. This date is between 1 and 731.
        :return verif_s_date: True if the date to convert belong to the same 
        year or to the year just after the first operation performed, 
        False otherwise
        :rtype jd: integer
        :rtype verif_s_date: boolean
    """
    
    m = datetime.strptime(s,'%Y/%m/%d').month
    d = datetime.strptime(s,'%Y/%m/%d').day
    y = datetime.strptime(s,'%Y/%m/%d').year
    y_mini = datetime.strptime(date_mini,'%Y/%m/%d').year
    jd = (date(y,m,d) - date(y,1,1)).days + 1
   
    # Find out whether the two dates belong to the same year.
    verif_s_date = True
    if y_mini != y :
        if y - y_mini == 1 :
            jd += (date(y_mini,12,31) - date(y_mini,1,1)).days + 1
        else :
            verif_s_date = False
            
    return jd,verif_s_date

def convert_code_residue(x) :
    """
        Convert the residue codes.
        :param x: ICASA residue code
        :type x: string
        :return: STICS residue code
        :rtype: string
    """
    
    list_re_STICS = range(1,6)+[7]  
    list_re_ICASA = ['RE001','RE002','RE003','RE005','RE004','RE203']
    if x in list_re_ICASA :
        return list_re_STICS[list_re_ICASA.index(x)]
    
    
def convert_code_fertilizer(x):
    """
        Convert the fertilizer codes.
        :param x: ICASA fertilizer code
        :type x: string
        :return: STICS fertilizer code
        :rtype: string
    """
    
    list_fe_STICS = range(1,8)
    list_fe_ICASA = ['FE001','FE010','FE005','FE004','FE002','FE007','FE008']
    if x in list_fe_ICASA :
        return list_fe_STICS[list_fe_ICASA.index(x)]

def default_values(dic_STICS):
    """
        Replace all the nan by default values and create the entries for the
        variables not refering to the template.
        :param dic_STICS: dictionary with STICS variables
        :type dic_STICS: dictionary
    """
    
    
    # Replace nan by default values for variables that are not dates and 
    # belonging to an intervention type.
    for x in dic_STICS :
       if isinstance(dic_STICS[x], list) :
           for el in dic_STICS[x]:
               if pa.isnull(el):
                   ind = dic_STICS[x].index(el)
                   dic_STICS[x][ind]= dic_def[x]
    
    # Replace nan by default values for the variables not belonging to an 
    # intervention type.
    for x in dic_STICS :
        if isinstance(dic_STICS[x], int) or isinstance(dic_STICS[x], str) \
        or isinstance(dic_STICS[x], float):
            if pa.isnull(dic_STICS[x]):
                dic_STICS[x]= dic_def[x]
    
    # If some non mandatory values not belonging to an intervention type 
    # are missing, the dic_def dictionary is used.  
    for x in dic_def:
        if x not in dic_STICS:
            dic_STICS[x]=dic_def[x]
     
            
def crea_dic_STICS(dic_temp,dic_def) :
    """
        Creation of the dic_STICS entries.
        :param dic_temp: the dictionary obtained from the dataframe with the 
        values converted with the right data type. 
        :param dic_def: dictionary with the STICS variables default values
        :type dic_temp: dictionary
        :type dic_def: dictionary
        :return dic_STICS: dictionary with the values that must appeared in the
        technical file
        :rtype dic_STICS: dictionary
            
    """
    
    dic_STICS = dict()
    
    # Retrieve data for number of interventions.
    
    dic_STICS['nb_amen'] = dic_temp['nb_OM'][0]
    dic_STICS['nb_till'] = dic_temp['TI_NO'][0]
    dic_STICS['nb_irri'] = dic_temp['IR_no'][0]
    dic_STICS['nb_fert'] = dic_temp['FEN_no'][0]
    dic_STICS['nb_cutt_days']=0
    dic_STICS['nb_cutt_degree_days']=0
    # Retrieve data for variables whose names begin with "STICS_". 
    # In this case, the end of the string gives the STICS_name. 
    for x in list(df) : 
        if re.match("STICS_+", x) :
            dic_STICS[x[6:]] = dic_temp[x][0]
            
    # Retrieve data for entries that don't requiere conversion.
    dic_STICS ['coderes']=dic_temp['OMCD'][:dic_STICS['nb_amen']]
    dic_STICS ['qres'] = dic_temp['OMCPC'][:dic_STICS['nb_amen']]
    dic_STICS ['CsurNres'] = dic_temp['OMC2N'][:dic_STICS['nb_amen']]
    dic_STICS ['Nminres'] = dic_temp['OMNPC'][:dic_STICS['nb_amen']]
    dic_STICS ['eaures'] = dic_temp['OMH2O'][:dic_STICS['nb_amen']]
    
    dic_STICS ['profres'] = dic_temp['STICS_profres'][:dic_STICS['nb_till']]
    dic_STICS ['proftrav'] = dic_temp['TIDEP'][:dic_STICS['nb_till']]
    dic_STICS ['amount'] = dic_temp['IRVAL'][:dic_STICS['nb_irri']]
    dic_STICS ['absolute_value'] = dic_temp['STICS_absolute_value']\
                                   [:dic_STICS['nb_fert']]
    
    dic_STICS ['Crespc']=[dic_def['Crespc']]*dic_STICS['nb_amen'] 
    
    dic_STICS ['effirr'] = dic_temp['IREFF'][0]
    dic_STICS ['Qtot_N'] = dic_temp['FEAMN'][0]
    dic_STICS ['locferti'] = dic_temp['FEDEP'][0]
    dic_STICS ['densitesem']= dic_temp['PLPOP'][0]
    
    # Create the dic_STICS entries for variables requiring conversion 
      
    #   - Convert qualitative variables codes.
   
    #   'engrais'
    if pa.isnull(dic_temp['FECD'][0]):
        dic_STICS['engrais'] = dic_def['engrais']
    else :
        dic_STICS['engrais'] = convert_code_fertilizer(dic_temp['FECD'][0])
        
     #   'coderes'
    dic_STICS['coderes'] = [convert_code_residue(el) \
                            for el in dic_temp['OMCD'][:dic_STICS['nb_amen']]]
        
    #     'codecalirrig'    irrigauto (1:yes, 2:no)
    if pa.isnull(dic_temp['IAME'][0]):
        dic_STICS['codecalirrig']= 2
    else :
        dic_STICS['codecalirrig']= 1
                                  
    #   - Convert quantitative variables :
    
    #       - Convert kg/ha to t/ha :  
    dic_STICS['qres'] = [el/1000 for el in dic_temp['OMAMT']\
                        [:dic_STICS['nb_amen']]]
    
    #       - Convert mm to cm
    dic_STICS ['profsem'] = dic_temp['PLDP'][0]/10
    
    
    """
    # Use PLMWT (kg/ha) and SDWT (g/plant) to calculate densitesem (plant.m-2)
    dic_STICS['densitesem']= (dic_temp['PLMWT'][0]/10)/dic_temp['SDWT'][0]
    """
    
    #  Convert the dates in julian days and assign them to the appropriate
    # STICS variables.
    eq_date = [['julres','date_OM'], ['jultrav','date_TI'],
               ['iplt0','date_PL'],['julapI_or_sumupvt','date_IR'],
               ['julapN_or_sum_upvt','date_FE'],
               ['irec','date_HA']]
    len_date = [dic_STICS['nb_amen'] , dic_STICS['nb_till'] , 1,
                dic_STICS['nb_irri'], dic_STICS['nb_fert'], 1]
    verif_d = True
    date_mini, var_min = date_min(dic_temp)
    for i in range(len(eq_date)) :
        dic_STICS[eq_date[i][0]] = [0]*len_date[i]
        for j in range(len_date[i]):
            if isinstance(dic_temp[eq_date[i][1]][j],str):
                dic_STICS[eq_date[i][0]][j],verif_s_date = \
                convert_date(dic_temp[eq_date[i][1]][j],date_mini)
                if verif_s_date == False :
                    verif_d = False
                    logger.info("For the variable {}, the date {} is"
                                " inconsistent. All the variables must belong"
                                " to either the same year or two consecutive "
                                " years. The first operation was performed on "
                                " {} and is related to the variable {}."
                                .format(eq_date[i][1],
                                dic_temp[eq_date[i][1]][j],date_mini,var_min))
            else :  
                dic_STICS[eq_date[i][0]][j]= dic_def[eq_date[i][0]]
    
    try : 
        assert verif_d == True
    except :
        raise Exception("Some dates are inconsistent.")
        
    dic_STICS['iplt0']=dic_STICS['iplt0'][0]
    dic_STICS['irec']=dic_STICS['irec'][0]
    
    # Complete the missing values.
    default_values(dic_STICS)
    
    return dic_STICS
   

#705714
def crea_file_STICS(file_name):
    """ 
     Create the STICS technical file.
     :param file_name: name of the technical file 
     :type file_name: string 
     """
    
    with open(file_name, 'w') as file_STICS :
        for el in list_unique_variables : 
            if re.match("nb_+", el): 
                file_STICS.write("nbinterventions\r\n")
                file_STICS.write(str(dic_STICS[el])+"\r\n")
                if dic_STICS[el] !=0:
                    ind = nb_int.index(el)
                    for i in range(dic_STICS[el]): 
                        file_STICS.write("opp1\r\n")
                        for x in opp[ind]:
                            if isinstance (dic_STICS[x][i],float):
                                file_STICS.write("%.2f" % dic_STICS[x][i]+" ")
                            else :
                                file_STICS.write(str(dic_STICS[x][i])+" ")
                        file_STICS.write("\r\n")
            else : 
                file_STICS.write(el+"\r\n") 
                if isinstance (dic_STICS[el],float):
                    file_STICS.write("%.5f" % dic_STICS[el]+"\r\n")
                else :
                    file_STICS.write(str(dic_STICS[el])+"\r\n")       


logger = logging.getLogger()
            	           		
""" MAIN """
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--itk_csv",required=True)
    parser.add_argument("-f","--fictec_txt",required=True)
    parser.add_argument("-l","--log_txt",required=True)
    args = parser.parse_args()
    crea_log(args.log_txt)   
    
    try:
        
        dic_def = crea_stics_dict_def()
        list_unique_variables = crea_stics_list_unique_var()
        nb_int, opp = crea_stics_list_opp()
        df = crea_data_array(args.itk_csv)
        dic_temp = crea_dic_template(df)
        dic_STICS = crea_dic_STICS(dic_temp,dic_def) 
        crea_file_STICS(args.fictec_txt)
        logger.info('Le fichier technique STICS a été créé.')
    except Exception as err:
        logger.exception(" %s, %s", err.__class__.__name__, err)
    close_log()  




