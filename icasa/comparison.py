#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 19 15:06:18 2018

@author: chlatorre
How to run this script : 
  python comparison.py -c converted_fictec_txt -e --expected_fictec_txt -d --diff_txt
"""
import argparse

def file_comparison(file_1,file_2,file_diff):
    """
    Create a file giving the differences between the original technical file 
    and the technical operations file generated by icasa_to_stics.py.
    :param file_1: file after icasa_to_stics
    :param file_2: expected file
    :param file_3: file giving the differences between file_1 and file_2
    :type file_1: string  
    :type file_2: string
    :type file_3: string
    """

    file_python = open(file_1, "r")
    list_file_python = file_python.readlines()
    file_python.close()
    
    original_file = open(file_2, "r")
    list_original_file = original_file.readlines()
    original_file.close()
    
    file_diff1 = open(file_diff,"w")
    i = 0
    while i <len(list_file_python):
        if list_file_python[i] != list_original_file[i]:
            if i%2==0 :
                file_diff1.write(list_file_python[i])
                file_diff1.write(list_file_python[i+1])
                file_diff1.write(list_original_file[i+1])
                i = i+1
            else: 
                file_diff1.write(list_file_python[i-1])
                file_diff1.write(list_file_python[i])
                file_diff1.write(list_original_file[i])
        i = i+1
        
    file_diff1.close()
    
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c","--converted_fictec_txt",required=True)
    parser.add_argument("-e","--expected_fictec_txt",required=True)
    parser.add_argument("-d","--diff_txt",required=True)
    args = parser.parse_args()    

    file_comparison(args.converted_fictec_txt,args.expected_fictec_txt,args.diff_txt)
