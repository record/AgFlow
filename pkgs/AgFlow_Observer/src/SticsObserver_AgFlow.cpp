/**
 * @file src/SticsObserver_AgFlow.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * @@tagdynamic@@
 * @@tagdepends:@@endtagdepends
 */


#include <vle/devs/Dynamics.hpp>
#include <vle/value/Value.hpp>
#include <vle/value/Set.hpp>
#include <vle/value/Double.hpp>

#include <vle/vpz/BaseModel.hpp> //vz::ConnectionList
#include <vle/utils/DateTime.hpp> //vu::DateTime::toJulianDayNumber

#include <iostream> //std::cout
#include <fstream> //std::ofstream
#include <sstream>  //std::stringstream

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
namespace vz = vle::vpz;

namespace AgFlow {

class SticsObserver_AgFlow : public vd::Dynamics
{
public:
    SticsObserver_AgFlow(const vd::DynamicsInit& init, const vd::InitEventList& events) : vd::Dynamics(init, events)
    {
		get_vpz_parameters(events);
		
		init_var_buffer(mVars);

		if (is_vpz_parameters_valid()) {
			throw vu::ModellingError("An error has occured during parameter checking");
        } else if (mDbgLog > 0.) {
			std::cout << "\t\t\t[SticsObserver_AgFlow] (constructor)\tparameters checking : OK" << std::endl;
		}
    }

    ~SticsObserver_AgFlow() override = default;

    vd::Time init(vd::Time /*time*/) override
    { return vd::infinity; /* model is only reactive (no autonomous behaviour)*/}

    void output(vd::Time /*time*/, vd::ExternalEventList& /*output*/) const override
    { }

    vd::Time timeAdvance() const override
    { return vd::infinity; /* model is only reactive (no autonomous behaviour)*/}

    void internalTransition(vd::Time /*time*/) override
    { }

    void externalTransition(const vd::ExternalEventList& events, vd::Time time) override
    {
		lastTime = time+begin_time;
		mVars_previous = mVars;
		double mPreviousIrecs = (time > 0.) ? mVars_previous["irecs"] : 0.;
		
        store_inputs(events, lastTime, mVars);
        
		if (is_end_usm(mVars["irecs"], mPreviousIrecs, mUsmInfoVect, lastTime)) { // detect end of usm day
			if (mDbgLog > 0.) {
				std::cout << vu::DateTime::toJulianDayNumber(lastTime) << "\t[SticsObserver_AgFlow](externalTransition)\t end of usm detected" << std::endl;
            }
            update_usm_infos(mUsmInfoVect, mUsmPortNames, mVars_previous, lastTime);
        }
        
        if (is_harvest(mVars["irecs"], mPreviousIrecs, time)){
			if (mDbgLog > 0.) {
				std::cout << vu::DateTime::toJulianDayNumber(lastTime) << "\t[SticsObserver_AgFlow](externalTransition)\t Harvest detected" << std::endl;
            }
            update_harvest_infos(mHarvestInfoVect, mHarvestPortNames, mVars, lastTime);
		}
        
    }

    void confluentTransitions(vd::Time time, const vd::ExternalEventList& events) override
    {
        internalTransition(time);
        externalTransition(events, time);
    }

    std::unique_ptr<vv::Value> observation(const vd::ObservationEvent& event) const override
    {
        for (std::map < std::string, double >::const_iterator it = mVars.begin(); it != mVars.end(); ++it) {
            if (event.onPort(it->first)) {
                return vv::Double::create(it->second);
            }
        }
        return 0;
    }

    void finish()
    {
		write_usm_output_file(mID + mUsmSuffix + ".txt", mUsmPortNames, mUsmInfoVect, lastTime);
		write_harvest_output_file(mID + mHarvestSuffix + ".txt", mHarvestPortNames, mHarvestInfoVect);
    }

private:

    //local variables
    std::map < std::string, double > mVars; // to store current daily values of the inputs received
    std::map < std::string, double > mVars_previous; // for harvest we need previous day values
     
    struct UsmInfo { // Info stored for each usm
        double EndUsmTime; // time of end of usm
        std::map < std::string, double > VarValues; // to store variables values
    };
    std::vector < UsmInfo > mUsmInfoVect; // vector of variables stored for each usm
    
    struct HarvestInfo { // Info stored for each harvest
        double HarvestTime; // time of harvest
        std::map < std::string, double > VarValues; // to store variables values
    };
    std::vector < HarvestInfo > mHarvestInfoVect; // vector of variables stored for each harvest
    
    double lastTime; // store the time for use in finish

    //parameters
    double mDbgLog; // debug message verbose level
    std::string mID; // unique identifier used as output filename prefix
    std::string mUsmSuffix; // suffix for output filename
    std::vector< std::string > mUsmPortNames; // name of the events to store at usm
    std::string mHarvestSuffix; // suffix for output filename
    std::vector< std::string > mHarvestPortNames; // name of the events to store at harvest

	std::string  begin_date;
	double begin_time;


	void write_usm_output_file(const std::string FileName, const std::vector< std::string > UsmPortNames, std::vector < UsmInfo > UsmInfoVect, const double LastTime) {
		std::stringstream ss;
		// write header
		ss << "ID;endUSM_date;endUSM_doy";
		for (std::vector< std::string >::const_iterator it=UsmPortNames.begin(); it!=UsmPortNames.end(); ++it) {
            ss  << ";" << (*it);
        }
        ss << std::endl;
        //write each usm info as a new line
        for (std::vector < UsmInfo >::iterator it = UsmInfoVect.begin();it!=UsmInfoVect.end(); ++it) {
            ss << mID << ";" << vu::DateTime::toJulianDayNumber((*it).EndUsmTime) << ";" << vu::DateTime::dayOfYear((*it).EndUsmTime);
            for (std::vector< std::string >::const_iterator it2=UsmPortNames.begin(); it2!=UsmPortNames.end(); ++it2) {
                ss << ";" << (*it).VarValues[(*it2)];
            }
            ss << std::endl;
        }
        if (UsmInfoVect.empty() || UsmInfoVect.back().EndUsmTime!=LastTime) { // add a new line for the end of simulation
            ss << mID << ";" << vu::DateTime::toJulianDayNumber(LastTime) << ";" << vu::DateTime::dayOfYear(LastTime);
            for (std::vector< std::string >::const_iterator it2=UsmPortNames.begin(); it2!=UsmPortNames.end(); ++it2) {
                ss << ";" << mVars[(*it2)];
            }
            ss << std::endl;
        }
 
        std::cout << "\t[SticsObserver_AgFlow](finish)\t writing usm file : " << FileName << std::endl;
        std::fstream outFile(FileName.c_str(), std::ios::out); // open output file for writing
        outFile << ss.str();
        outFile.close();
	}
	
	void write_harvest_output_file(const std::string FileName, const std::vector< std::string > HarvestPortNames, std::vector < HarvestInfo > HarvestInfoVect) {
		std::stringstream ss;
            // write header
            ss << "ID;harvest;harvest_doy";
            for (std::vector< std::string >::const_iterator it=HarvestPortNames.begin(); it!=HarvestPortNames.end(); ++it) {
                ss  << ";" << (*it);
            }
            ss << std::endl;
            //write each harvest info as a new line
            for (std::vector < HarvestInfo >::iterator it = HarvestInfoVect.begin();it!=HarvestInfoVect.end(); ++it) {
                if ((*it).HarvestTime!=-999.) {
                    ss << mID << ";" << vu::DateTime::toJulianDayNumber((*it).HarvestTime) << ";" << vu::DateTime::dayOfYear((*it).HarvestTime);
                    for (std::vector< std::string >::const_iterator it2=HarvestPortNames.begin(); it2!=HarvestPortNames.end(); ++it2) {
                        ss << ";" << (*it).VarValues[(*it2)];
                    }
                    ss << std::endl;
                }
            }
            std::cout << "\t[SticsObserver_AgFlow](finish)\t writing harvest file : " << FileName << std::endl;
            std::fstream outFile(FileName.c_str(), std::ios::out); // open output file for writing
            outFile << ss.str();
            outFile.close();
	}
	
	bool is_end_usm(const double irecs, const double previous_irecs, const std::vector < UsmInfo > UsmInfoVect, const vd::Time& Time) {
		return( (irecs == 0 && previous_irecs > 0) || (!UsmInfoVect.empty() && UsmInfoVect.back().EndUsmTime==Time)); // also detect end of simulation
	}
	
	bool is_harvest(const double irecs, const double previous_irecs, const double Time) {
		return(previous_irecs == 0 && irecs > 0 && Time > 1.);
	}
	
	void get_vpz_parameters (const vd::InitEventList& events) {
		std::cout << "\t\t\t[SticsObserver_AgFlow] (getVPZParameters)\tReading vpz parameters" << std::endl;
		if (events.get("begin_date")->isDouble()) {
			begin_time = events.getDouble("begin_date");
			begin_date = vu::DateTime::toJulianDay(begin_time);
		} else if (events.get("begin_date")->isString()) {
			begin_date = events.getString("begin_date");
			begin_time = vu::DateTime::toJulianDay(begin_date);
		}
		
        mDbgLog = (events.exist("DbgLog")) ? vv::toDouble(events.get("DbgLog")) : 0.;
        mID = (events.exist("SticsId")) ? vv::toString(events.get("SticsId")) : "";

        mUsmSuffix = (events.exist("UsmSuffix")) ? vv::toString(events.get("UsmSuffix")) : "_UsmOutputs";
        mHarvestSuffix = (events.exist("HarvestSuffix")) ? vv::toString(events.get("HarvestSuffix")) : "_HarvestOutputs";   

        const vv::Set& UsmVarNames = events.getSet("UsmVarNames");
        for (unsigned int index = 0; index < UsmVarNames.size(); ++index) {
            mUsmPortNames.push_back(UsmVarNames.getString(index));
            if (mDbgLog > 0.) {
                std::cout << "\t\t\t[SticsObserver_AgFlow] (getVPZParameters)\tVar for Usm : " << mUsmPortNames.back() << std::endl;
            }
        }
        
        const vv::Set& HarvestVarNames = events.getSet("HarvestVarNames");
        for (unsigned int index = 0; index < HarvestVarNames.size(); ++index) {
            mHarvestPortNames.push_back(HarvestVarNames.getString(index));
            if (mDbgLog > 0.) {
                std::cout << "\t\t\t[SticsObserver_AgFlow] (getVPZParameters)\tVar for Harvest : " << mHarvestPortNames.back() << std::endl;
            }
        }		
	}
	
	bool is_vpz_parameters_valid () {
		if (mDbgLog > 0.) {
			std::cout << "\t\t\t[SticsObserver_AgFlow] (is_vpz_parameters_valid)\tChecking parameters validity" << std::endl;
		}
		
		bool error=false;
		    // check input port availability vs cond.exp UsmVarNames
            for (std::vector< std::string >::const_iterator it=mUsmPortNames.begin(); it!=mUsmPortNames.end(); ++it) {
                if (mVars.find(*it)==mVars.end()) {
                    error = true;
                    if (mDbgLog > 0.) {
                        std::cout << "[Error]\trequested Input port "<< (*it) <<" missing (UsmVarNames)" << std::endl;
                    }
                }
            }
            // check input port availability vs cond.exp HarvestPortNames
            for (std::vector< std::string >::const_iterator it=mHarvestPortNames.begin(); it!=mHarvestPortNames.end(); ++it) {
                if (mVars.find(*it)==mVars.end()) {
                    error = true;
                    if (mDbgLog > 0.) {
                        std::cout << "[Error]\trequested Input port "<< (*it) <<" missing (HarvestPortNames)" << std::endl;
                    }
                }
            }
			
            //for harvest and end of usm detection
            if (mVars.find("irecs") == mVars.end()) {
                error = true;
                if (mDbgLog > 0.) {
                    std::cout << "[Error]\tInput port irecs missing" << std::endl;
                }
            }
        return(error);
	}
	
	// detect all input ports and create a corresponding element of VarsBuffer for each
	void init_var_buffer(std::map < std::string, double > &VarsBuffer) {
		const vz::ConnectionList& list = getModel().getInputPortList();
		for (vz::ConnectionList::const_iterator it = list.begin(); it != list.end(); ++it) {
			VarsBuffer[it->first] = -999.;
			if (mDbgLog > 0.) {
				std::cout << "\t\t\t[SticsObserver_AgFlow] (init_var_buffer)\tdetect input port:" << it->first << std::endl;
            }
        }
    }
    
    void store_inputs(const vd::ExternalEventList& evts, const double Time, std::map < std::string, double > &VarsBuffer) {
		for (vd::ExternalEventList::const_iterator it = evts.begin(); it != evts.end(); ++it) {
            std::string name = it->getPortName(); 
            double value = it->attributes()->toDouble().value();
            VarsBuffer[name] = value;
            if (mDbgLog > 1.) {
                std::cout << vu::DateTime::toJulianDayNumber(Time) << "\t[SticsObserver_AgFlow](externalTransition)\tevent received:\t" << name << "\t=\t" << value << std::endl;
            }
        }
	}
	
	
	void update_usm_infos(std::vector < UsmInfo > &UsmInfoVect, const std::vector< std::string > UsmPortNames, std::map < std::string, double > VarsBuffer, const double Time) {
		if (UsmInfoVect.empty() || UsmInfoVect.back().EndUsmTime!=Time-1) {
			UsmInfo newUsmLine;
            newUsmLine.EndUsmTime = Time-1;
            for (std::vector< std::string >::const_iterator it=UsmPortNames.begin(); it!=UsmPortNames.end(); ++it) {
                newUsmLine.VarValues[*it] = VarsBuffer[*it];
            }
            UsmInfoVect.push_back(newUsmLine);
        } else {
            for (std::vector< std::string >::const_iterator it=UsmPortNames.begin(); it!=UsmPortNames.end(); ++it) {
                UsmInfoVect.back().VarValues[*it] = VarsBuffer[*it];
            }
        }
	}
	
	void update_harvest_infos(std::vector < HarvestInfo > &HarvestInfoVect, const std::vector< std::string > HarvestPortNames, std::map < std::string, double > VarsBuffer, const double Time) {
		HarvestInfo newHarvestLine;
		newHarvestLine.HarvestTime = Time;
        for (std::vector< std::string >::const_iterator it=HarvestPortNames.begin(); it!=HarvestPortNames.end(); ++it) {
			newHarvestLine.VarValues[*it] = VarsBuffer[*it];
		}
		HarvestInfoVect.push_back(newHarvestLine);
	}
	
};

} // namespace AgFlow

DECLARE_DYNAMICS(AgFlow::SticsObserver_AgFlow)
