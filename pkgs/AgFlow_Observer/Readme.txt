Requirements
============

vle      2.0  http://www.vle-project.org
boost    1.49 http://www.boost.org
cmake    2.8  http://www.cmake.org


Installation
============

$ vle --package AgFlow_Observer configure
$ vle --package AgFlow_Observer build
